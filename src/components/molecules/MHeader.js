import React from 'react'
import { Flex, Box, Heading, Button, Spacer, Text } from '@chakra-ui/react'
import { AColorModeSwitcher } from '../atoms/AColorModeSwitcher'
const MHeader = () => {
  return (
    <Flex justifyContent={'center'} alignItems={'center'}>
      <Box p='5'>
        <Heading size='md'>Chakra App</Heading>
      </Box>
      <Spacer />
      <Box>
        <Button colorScheme='teal' variant='ghost' mx={1}>
          About us
        </Button>
        <Button colorScheme='teal' variant='ghost' mx={1}>
          How to use
        </Button>
        <Button colorScheme='teal' mx={1}>Download</Button>
        <AColorModeSwitcher />
      </Box>
    </Flex>
  )
}

export default MHeader
