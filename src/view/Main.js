import React from 'react'

import { Container } from '@chakra-ui/react'
import MHeader from '../components/molecules/MHeader'

const main = () => {
  return (
    <Container maxW='container.xl'>
      <MHeader />
    </Container>
  )
}

export default main
